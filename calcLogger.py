#! /usr/bin/env python
# -*- coding: utf-8 -*-
import re
import json
from datetime import datetime

import click
import requests

import numpy as np
import pandas as pd

from saqc import UNFLAGGED, DictOfSeries, fromConfig, SaQC
from saqc.core import flagging, register, Flags

from pipetools.lib import splitData, mergeData, writeParquet


NM_URL = "http://www.nmdb.eu/nest/draw_graph.php"


def getNMData(station: str, resolution: int, start_date: pd.Timestamp, end_date: pd.Timestamp) -> pd.Series:

    start_date = start_date.to_period("A").to_timestamp()
    end_date = (end_date
                .to_period("A")
                .to_timestamp("A")
                .replace(hour=23, minute=59))

    return nmDownload(
        station=station, resolution=resolution,
        start_date=start_date, end_date=end_date
    )


def nmDownload(station: str, resolution: int, start_date: datetime, end_date: datetime) -> pd.Series:

    params = {
        "wget": 1,
        "stations[]": station,
        "tabchoice": "revori",
        "dtype": "corr_for_efficiency",
        "tresolution": resolution,
        "force": 1,
        "date_choice": "bydate",
        "start_year": {start_date.year},
        "start_month": {start_date.month},
        "start_day": {start_date.day},
        "start_hour": {start_date.hour},
        "start_min": {start_date.min},
        "end_year": {end_date.year},
        "end_month": {end_date.month},
        "end_day": {end_date.day},
        "end_hour": {end_date.hour},
        "end_min": {end_date.min},
        "yunits": 0
    }

    res = requests.get(NM_URL, params=params)
    rows = re.findall(r"^\d.*", res.text, flags=re.MULTILINE)
    values = pd.DataFrame(
        [r.split(";") for r in rows],
        columns=["date", "NM"]
    )
    out = values["NM"].astype(float)
    out.index = pd.to_datetime(values["date"]) #, utc=True)
    return out


def extractNM(index):
    resolution = 60
    nm = getNMData(
        station="JUNG", resolution=resolution,
        start_date=index.min(), end_date=index.max()
    )
    nm = nm.reindex(
        index, method="nearest", limit=1,
        tolerance=pd.Timedelta(seconds=resolution*60/2-1)
    ).interpolate()
    return nm


@register(mask=["field"], demask=["field"], squeeze=[], handles_target=True)
def calcSoilMoisture(
        qc: SaQC,
        field: str,
        target: str,
        n0: int,
        lattice_water: float,
        bulk_density: float,
        soil_org_carbon: float,
        # basically constants
        a0: float = 0.0808,
        a1: float = 0.372,
        a2: float = 0.115,
        **kwargs
) -> SaQC:

    neutrons = qc._data[field]
    sm = (a0 / (neutrons / n0 - a1) - a2 - lattice_water - soil_org_carbon * 0.556) * bulk_density
    qc._data[target] = sm
    qc._flags[target] = pd.Series(UNFLAGGED, index=sm.index)
    return qc


def getManFlags(fname, field):

    mflags = pd.read_csv(fname, sep=";", comment="#")
    mflags = mflags.apply(lambda s: s.str.strip())
    mflags.columns = mflags.columns.str.strip()
    mflags = mflags.set_index("variable", drop=True)

    out = mflags[mflags.index == field]
    out["start_date"] = pd.to_datetime(out["start_date"])
    out["end_date"] = pd.to_datetime(out["end_date"])
    return out


@flagging()
def flagFromFile(qc: SaQC, field: str, fname: str, **kwargs) -> SaQC:
    mflags = getManFlags(fname, field)
    for _, (start, end, flag) in mflags.iterrows():
        mask = pd.Series(data=0, index=data[field].index, dtype=bool)
        mask.loc[start:end] = True
        qc._flags[mask, field] = flag
    return qc


def resultStack(data, flags):
    assert (data.columns == flags.columns.get_level_values(0).drop_duplicates()).all()
    t0 = datetime.now()
    variables = []
    for col in data.columns:
        var = flags[col]
        var.loc[:, "data"] = data[col].round(8)
        var.index = pd.MultiIndex.from_product([[col], var.index])
        variables.append(var)
    return pd.concat(variables)

def diosUnstack(data: pd.DataFrame, level=0) -> DictOfSeries:
    data = data.copy()
    data_map = {}
    for k in data.index.get_level_values(level).drop_duplicates():
        data_map[k] = data.loc[k].squeeze()
    return DictOfSeries(data_map)

@click.command()
@click.option("-i", "--infile", type=click.Path(exists=True), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
@click.option("-c", "--configfile", type=click.Path(exists=True), required=True)
def main(infile, outfile, configfile):

    data = pd.read_parquet(infile)
    dios = diosUnstack(data)

    index = data.index.get_level_values("timestamp").drop_duplicates().sort_values()
    dios["NM"] = extractNM(index)

    saqc = fromConfig(fname=configfile, data=dios, scheme="dmp")

    df_out = resultStack(saqc.data, saqc.flags)

    writeParquet(df_out, outfile)


if __name__ == "__main__":
    main()
