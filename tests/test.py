#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
from pathlib import Path

import yaml
import pandas as pd

from dmpapi import DmpAPIPandas


CONFIG_FILE = "config.yaml"
CONFIG_PATH = "config"
MANUAL_PATH = "manual"


def getSnakemakeConfig(key=None):
    with open(CONFIG_FILE) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    if key is not None:
        return config[key]
    return config

def getSaQCFiles(path):
    return [str(f) for f in Path(path).iterdir()]

def readSaQCConfig(fname):
    df = pd.read_csv(fname, sep=";", comment="#")
    df.columns = df.columns.str.strip()
    return df.apply(lambda c: c.str.strip())


def test_configfileExists():

    def matchLoggerId(lid, files):
        check = [re.search(f"\\b{lid}\\b", f) for f in files]
        return len(tuple(filter(None, check)))

    for lid in getSnakemakeConfig("loggers"):

        n_configs = matchLoggerId(lid, getSaQCFiles(CONFIG_PATH))
        assert (
            n_configs == 1
        ), f"expected exactly one config file for logger '{lid}' (got: {n_configs}) "

        n_manuals = matchLoggerId(lid, getSaQCFiles(MANUAL_PATH))
        assert (
            n_manuals == 1
        ), f"expected exactly one config file for logger '{lid}' (got: {n_manuals}) "


# def test_validSaQCConfigs():
#     for src_lid, trg_lid in getSnakemakeConfig("loggers").items():
#         fname = Path(CONFIG_PATH) / f"saqc-proc-{src_lid}.csv"
#         df = readSaQCConfig(fname)
#         varnames = df["varname"].str.strip()
#         varnames = varnames[~varnames.str.startswith("'")]
#         api = DmpAPIPandas(getSnakemakeConfig("project"), os.environ["DMPUSER"], os.environ["DMPPASS"])
#         src_sensors = api.getSensors(src_lid)["sensorLabel"]
#         trg_sensors = api.getSensors(trg_lid)["sensorLabel"]
#         diff = set(varnames) - set(src_sensors) - set(trg_sensors)
#         assert len(diff) == 0, f"unexpected varname(s) '{diff}' used in configuration file '{fname}'"


def test_validSaQCManual():
    for src_lid in getSnakemakeConfig("loggers"):
        fname = Path(MANUAL_PATH) / f"saqc-flags-{src_lid}.csv"
        df = readSaQCConfig(fname)
        if df.empty:
            continue
        varnames = df["varname"].str.strip()
        varnames = varnames[~varnames.str.startswith("'")]
        api = DmpAPIPandas(getSnakemakeConfig("project"), os.environ["DMPUSER"], os.environ["DMPPASS"])
        src_sensors = api.getSensors(src_lid)["sensorLabel"]
        diff = set(varnames) - set(src_sensors)
        assert len(diff) == 0, f"unexpected varname(s) '{diff}' used in manual flagging file '{fname}'"
