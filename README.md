# Adding a new logger
In order to include a new logger into the processing, two differnt files need to merged into this repository (using a merge request): 
1. A [SaQC](https://git.ufz.de/rdm-software/saqc) configuration file, this file needs to be stored within the subdirectory `config` and should follow the naming convention `saqc-proc-{logger-id}.csv`, were `{logger-id}` needs to be replaced with the ID of the respective logger in [the Datenmanagementportal](https://www.intranet.ufz.de/admin.php?de=90002&adm_app=95)
2. A manual flag file, stored within in the subdiretory `manual` and folling the naming convention `saqc-flags-{logger-id}.csv`

# Requirements
The Pipeline expects the [DMP-API](https://git.ufz.de/rdm/dmpapi/) to be up and
available, host and port to the running API-Service can be given as command line
arguments to `pipeline.py`

There a number of 'secrets' needed to run the pipeline:
1.  DMP credentials to access the database
2.  Email credentials to send logging output per email

The application needs these values either as command line arguments `--apiuser`,
`--apipass` and `--emailuser`, `--emailpass` or as environmental variables 
`APIUSER`, `APIPASS` and `EMAILUSER`, `EMAILPASS`. The recommended way to pass
these parameters into the container is by an file listing the environmental 
variables as 
```sh
APIUSER=apiusername
APIPASS=apipassword
EMAILUSER=emailusername
EMAILPASS=emailpassword
```
and pass it to the `docker run` comand using the option `--env-file filename`

# Usage

## custom build
```sh
docker build --network host -t crns-pipeline .
docker run --env-file /PATH/TO/ENVFILE --mount type=bind,source=/LOCAL/DIRECTORY,target=/.cache --env-file /PATH/TO/ENVFILE --rm crns-pipeline
```

## pre built
```sh
docker pull git.ufz.de:4567/rdm/pipelines/crns:latest
docker run --env-file /PATH/TO/ENVFILE --mount type=bind,source=/LOCAL/DIRECTORY,target=/.cache --network host --rm git.ufz.de:4567/rdm/pipelines/crns:latest
```
