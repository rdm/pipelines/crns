FROM python:3.9-slim

RUN apt-get update && apt-get install -y --no-install-recommends git gcc python3-dev

RUN git clone --recursive https://git.ufz.de/rdm/pipelines/crns /pipeline

WORKDIR pipeline

ENV PYTHONPATH=saqc/:pipetools/:$PYTHONPATH

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r saqc/requirements.txt
RUN pip install --no-cache-dir snakemake

CMD snakemake -j2 --forceall --printshellcmds --keep-going --restart-times 3
