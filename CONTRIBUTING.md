## General

- All contributions, whether changes to configuration or manual flag files or code changes, need to be integrated as merge requests.
- Merges into `master` trigger a new pipeline container build, which will be used during the next pipeline run. Errors introduced here, will be deployed and result in broken pipeline runs. So please take extra care!
