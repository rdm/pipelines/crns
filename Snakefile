
configfile: "./config.yaml"

PATH = config["path"]
PROJECT_ID = config["project"]
LOGGER_IDS = config["loggers"]


wildcard_constraints:
    logger_id="\d+"


def getLoggerConfig():
    # CLI usage: '--config logger_id=1[,2,...]', only specify the root logger

    all_ids = set(LOGGER_IDS.keys())
    given_ids = config.get("logger_id", None)

    if given_ids:
        given_ids = set(map(int, str(given_ids).split(",")))
    else:
        given_ids = all_ids

    if given_ids - all_ids:
        diff = list(given_ids - all_ids)
        raise ValueError(f"Unknown logger id(s): {diff}")
    return given_ids


def getTargets():
    given_ids = getLoggerConfig()

    out = []
    for k, v in LOGGER_IDS.items():
        if k not in given_ids:
            continue

        out.append(f"{PATH}/{k}_qs.upload")
        if v:
            out.append(f"{PATH}/{k}_proc.upload")
    return out


rule all:
    input: getTargets()


rule extractLevel1:
    params: "{logger_id}"
    output: "{PATH}/{logger_id}_level1.parquet"
    shell:  "python pipetools/extractLogger.py --project {PROJECT_ID} --logger {params} --level 1 --outfile {output}"

rule extractLevel2:
    params: "{logger_id}"
    output: "{PATH}/{logger_id}_level2.parquet"
    shell:  "python pipetools/extractLogger.py --project {PROJECT_ID} --logger {params} --level 2 --outfile {output}"

rule calc:
    input: "{PATH}/{logger_id}_level1.parquet",
    output: "{PATH}/{logger_id}_saqc.parquet"
    shell:  "python calcLogger.py --infile {input} --outfile {output} --configfile config/saqc-proc-{wildcards.logger_id}.csv"

rule loadQS:
    params: "{logger_id}"
    input: "{PATH}/{logger_id}_saqc.parquet"
    output: touch("{PATH}/{logger_id}_qs.upload")
    shell: "python pipetools/loadLogger.py --infile {input} --project {PROJECT_ID} --logger {params} --level 2 --no-meta-data"

rule loadProcessed:
    params: lambda wc, *args: LOGGER_IDS[int(wc[1])]
    input: "{PATH}/{logger_id}_saqc.parquet",
    output: touch("{PATH}/{logger_id}_proc.upload")
    shell: "python pipetools/loadLogger.py --infile {input} --project {PROJECT_ID} --logger {params} --level both --no-meta-data"


onsuccess:
    shell("python pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject 'CRNS Pipeline: FINISHED'")


onerror:
    shell("cat {log} | python pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject 'CRNS Pipeline: FAILED' --stdin")
