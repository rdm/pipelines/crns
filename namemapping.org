* Sensornamen/-labels
  | dmp                                                        | data      | label |
  |------------------------------------------------------------+-----------+-------|
  | NumberingValuenumber0mSingle_value15Min                    | RecordNum | id    |
  | WeatherAirpressure0mSingle_value15MinP1                    | P1_mb     | p1    |
  | WeatherAirpressure0mAvg15MinP3                             | P3_mb     | p3    |
  | WeatherAirpressure0mSingle_value15MinP4                    | P4_mb     | p4    |
  | DevicesTemperaturedatalogger0mSingle_value15MinT1          | T1_C      | T1    |
  | DevicesTemperaturedatalogger0mSingle_value15MinT2          | T2_C      | T2    |
  | DevicesTemperaturedatalogger0mSingle_value15MinT3          | T3_C      | T3    |
  | DevicesTemperaturedatalogger0mSingle_value15MinT4          | T4_C      | T4    |
  | WeatherAirtemperature1mSingle_value15Min                   | T_CS215   | TX    |
  | DevicesRelativehumiditydevice0mSingle_value15MinRh1        | RH1       | RH1   |
  | DevicesRelativehumiditydevice0mSingle_value15MinRh2        | RH2       | RH2   |
  | WeatherRelativehumidity1mSingle_value15Min                 | RH_CS215  | RHX   |
  | DevicesVoltagelogger0mSingle_value15Min                    | Vbat      | Vbat  |
  | SoilNeutroncounts1mSingle_value15MinEpithermal             | N1Cts     | Nf    |
  | SoilNeutroncounts1mSingle_value15MinThermal                | N2Cts     | Nt    |
  | TimeHktime0mSingle_value15MinEpithermal                    | N1ET_sec  | tNf   |
  | TimeHktime0mSingle_value15MinThermal                       | N2ET_sec  | tNt   |
  | DevicesTemperaturedevice1mSingle_value15Min                | N1T_C     | NfT   |
  | DevicesRelativehumiditydevice1mSingle_value15MinEpithermal | N1RH      | NfRH  |
  | DevicesTemperaturedevice1mSingle_value15MinThermal         | N2T_C     | NtT   |
  | DevicesRelativehumiditydevice1mSingle_value15MinThermal    | N2RH      | NtRH  |
  | DevicesImpulsescounts1mSingle_value15Min                   | D1        | D1    |

* Neue anzulegende Variablen pro Logger
  | prog_name             |
  |-----------------------|
  | N                     |
  | N_thermal             |
  | NM                    |
  | ah                    |
  | correct_h             |
  | correct_p             |
  | correct_inc           |
  | N_cleaned             |
  | SM                    |
  | SM_%                  |
  | (N_corrected)         |
  | (N_thermal_corrected) |
